#include "instructions.h"

TmInstruction::TmInstruction(InstructionCode code, int res,  int src, int arg) :
code(code),resultReg(res),sourceReg(src),arg(arg){}

TmInstruction * TmInstruction::generate(InstructionCode code,int result,int source,int arg)
{
    if(code<=InstructionCode::DIV)
      return new RrInstruction(code,result,source,arg);
    else if(code>InstructionCode::DIV && code<=InstructionCode::ST)
      return new RmInstruction(code,result,source,arg);
    else
      return new RaInstruction(code,result,source,arg);
}

void RmInstruction::writeToStream(std::ostream & os) const
{
  switch(code)
  {
    case LD:
      os<<sLD<<"\t";
      break;
    case ST:
      os<<sST<<"\t";
      break;
    default:
      break;
  }
  
  os<<resultReg<<","<<arg<<"("<<sourceReg<<")";
}

RaInstruction::RaInstruction(InstructionCode code, int res,  int src, int arg) :
TmInstruction(code,res,src,arg){}

RmInstruction::RmInstruction(InstructionCode code, int res,  int src, int arg) :
TmInstruction(code,res,src,arg){}

RrInstruction::RrInstruction(InstructionCode code, int res,  int src, int arg) :
TmInstruction(code,res,src,arg){}

void RaInstruction::writeToStream(std::ostream & os) const
{
  switch(code)
  {
    case LDA:
      os<<sLDA<<"\t";
      break;
    case LDC:
      os<<sLDC<<"\t";
      break;
    case JLT:
      os<<sJLT<<"\t";
      break;
    case JLE:
      os<<sJLE<<"\t";
      break;
    case JGT:
      os<<sJGT<<"\t";
      break;
    case JGE:
      os<<sJGE<<"\t";
      break;
    case JEQ:
      os<<sJEQ<<"\t";
      break;
    case JNE:
      os<<sJNE<<"\t";
      break;      
    default:
      break;
  }
  
  os<<resultReg<<","<<arg<<"("<<sourceReg<<")";
}

void RrInstruction::writeToStream(std::ostream & os) const
{
  switch(code)
  {
    case HALT:
      os<<sHALT<<"\t";
      break;
      
    case IN:
      os<<sIN<<"\t";
      break;
    case OUT:
      os<<sOUT<<"\t";
      break;
    case ADD:
      os<<sADD<<"\t";
      break;
    case INC:
      os<<sINC<<"\t";
      break;
    case DEC:
      os<<sDEC<<"\t";
      break;
    case SHR:
      os<<sSHR<<"\t";
      break;
    case SHL:
      os<<sSHL<<"\t";
      break;
    case SUB:
      os<<sSUB<<"\t";
      break;    
    case MUL:
      os<<sMUL<<"\t";
      break;    
    case DIV:
      os<<sDIV<<"\t";
      break;          
    default:
      break;
  }
  
  os<<resultReg<<","<<sourceReg<<","<<arg;
}

std::ostream & operator<< (std::ostream & os, const TmInstruction& ins)
{
    ins.writeToStream(os);
  
    return os;
}