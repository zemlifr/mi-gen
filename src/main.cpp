/* main.c */
/* syntakticky analyzator */

#include "lexan.h"
#include "parser.h"
#include "strom.h"
#include "zaspoc.h"
#include <stdio.h>
#include <cstring>

int main(int argc, char *argv[])
{
   char * inputFile = NULL;
   char * outputFile = NULL;
   printf("Syntakticky analyzator\n");
   if (argc == 1) 
   {
      printf("Vstup z klavesnice, zadejte zdrojovy text\n");
   } 
   else if(argc == 2)
   {
      inputFile = argv[1];
      printf("Vstupni soubor %s\n", inputFile);
   }
   else if(argc == 3)
   {
     if(strcmp(argv[1],"-o") == 0)
     {
     }
       
   }
   else if(argc == 4)
   {
     
   }
   InitLexan(inputFile);
   CtiSymb();
   Prog *prog = Program();
   prog = (Prog*)(prog->Optimize());
   prog->Translate();
   Print();
   Run();
   printf("Konec\n");
}
