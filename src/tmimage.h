#ifndef TM_IMAGE_H
#define TM_IMAGE_H

#include <ostream>
#include <vector>

#include "instructions.h"

// register alias definition
#define PC 7

class TmImage
{
private:
  std::vector<TmInstruction*> memory;
  int ic;
  
public:
  TmImage();
  ~TmImage();
  int putInstruction(TmInstruction *ins);
  int getIC()const;
  
  friend std::ostream & operator<< (std::ostream & os, const TmImage& image);
};

#endif /*TM_IMAGE_H*/