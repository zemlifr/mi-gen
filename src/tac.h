#ifndef TAC_H
#define TAC_H

#include <vector>
#include <map>

#include "tmimage.h"
#include "instructions.h"

class Tac
{
private:
  std::vector<TmInstruction*> code;
  std::map<int,int> relativeJumps;
  
public:
   ~Tac();
   TmImage * generateTmImage();
   int putInstruction(TmInstruction *ins);
};

#endif //TAC_H