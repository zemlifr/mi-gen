#include "tmimage.h"

TmImage::TmImage() : ic(0){}

TmImage::~TmImage()
{
    for(auto it = memory.begin();it!=memory.end();++it)
    {
      delete *it;
    }
}

int TmImage::getIC()const
{
  return ic;
}

int TmImage::putInstruction(TmInstruction *ins)
{
  memory.push_back(ins);
  ic++;
  
  return ic-1;
}

std::ostream & operator<< (std::ostream & os, const TmImage& image)
{
    int ic = 0;
  
    for(auto it = image.memory.begin();it!=image.memory.end();++it)
    {
	os<<ic<<":\t"<<**it<<"\n";
	
	ic++;
    }
    
    return os;
}
