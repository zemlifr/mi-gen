#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

//instruction definition
#define sHALT "HALT"
#define sIN "IN"
#define sOUT "OUT"
#define sADD "ADD"
#define sINC "INC"
#define sDEC "DEC"
#define sSHR "SHR"
#define sSHL "SHL"
#define sSUB "SUB"
#define sMUL "MUL"
#define sDIV "DIV"

#define sLD "LD"
#define sST "ST"

#define sLDA "LDA"
#define sLDC "LDC"
#define sJLT "JLT"
#define sJLE "JLE"
#define sJGT "JGT"
#define sJGE "JGE"
#define sJEQ "JEQ"
#define sJNE "JNE"


#include <ostream>

enum InstructionCode {HALT,IN,OUT,ADD,INC,DEC,SHR,SHL,SUB,MUL,DIV,LD,ST,LDA,LDC,JLT,JLE,JGT,JGE,JEQ,JNE};

class TmInstruction
{
public:
  InstructionCode code;
  int resultReg;
  int sourceReg;
  int arg;

  TmInstruction(InstructionCode code, int res,  int src, int arg);
  virtual void writeToStream(std::ostream & os) const=0;
  static TmInstruction * generate(InstructionCode code,int result,int source,int arg);
  friend std::ostream & operator<< (std::ostream & os, const TmInstruction& ins);
};

class RrInstruction : public TmInstruction
{
public:
  RrInstruction(InstructionCode code, int res,  int src, int arg);
  virtual void writeToStream(std::ostream & os) const;
};

class RaInstruction : public TmInstruction
{
public:
  RaInstruction(InstructionCode code, int res,  int src, int arg);
  virtual void writeToStream(std::ostream & os) const;
};

class RmInstruction : public TmInstruction
{
public:
  RmInstruction(InstructionCode code, int res,  int src, int arg);
  virtual void writeToStream(std::ostream & os) const;  
};

#endif //INSTRUCTIONS_H